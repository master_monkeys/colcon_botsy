#!/bin/bash
#

# Run drone gazebo script
gnome-terminal --tab -- bash ~/colcon_botsy/scripts/run_px4_drone_gazebo_world.bash

# Run sitl script
gnome-terminal --tab -- bash ~/colcon_botsy/scripts/run_px4_sitl_gazebo.bash

# Open Rviz2 in ros2
gnome-terminal --tab -- ros2 run rviz2 rviz2 -d ~/colcon_botsy/rviz/botsy_PointCloud_config.rviz

# Open QGroundControl
cd ~/qgc/
gnome-terminal --tab -- ./QGroundControl.AppImage 
cd

# Launch ros2 idl transform node
source ~/colcon_botsy/scripts/setup_ros_gazebo.bash
ros2 run idl_transform_pkg ros_node_tf_odom
