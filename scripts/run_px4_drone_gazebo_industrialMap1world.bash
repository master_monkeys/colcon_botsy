#!/bin/bash
#
# Runs gazebo simulation

cd
source ~/colcon_botsy/scripts/setup_ros_gazebo.bash
cd ~/gazebo_botsy/worlds

ros2 launch gazebo_ros gazebo.launch.py world:=industrialMap_world.world
