#!/bin/bash
#
# Run sitl gazebo simulation for ros2 firmware

cd ~/PX4-Autopilot
no_sim=1 make px4_sitl_rtps gazebo -j$(nproc)
