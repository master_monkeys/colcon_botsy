#!/bin/bash

# Launch ros2 idl transform node
source ~/colcon_botsy/scripts/setup_ros_gazebo.bash
ros2 run idl_transform_pkg ros_node_groundTruth_publisher
