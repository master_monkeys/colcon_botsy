#!/bin/bash
#
# Setup to set gazebo paths and source ros2 ws

source ~/gazebo_botsy/setup_gazebo.bash
echo -e "Gazebo setup OK!"

source ~/colcon_botsy/install/setup.bash
echo -e "ROS2 setup OK!"

