#!/bin/bash


# Run drone gazebo script
gnome-terminal --tab -- bash ~/colcon_botsy/scripts/run_px4_drone_gazebo_uiaBasement.bash

# Run sitl script
gnome-terminal --tab -- bash ~/colcon_botsy/scripts/run_px4_sitl_gazebo.bash

# Launch ros2 idl transform node
gnome-terminal --tab -- bash ~/colcon_botsy/scripts/run_ros2_gazebo_groundTruth.bash

# Open QGroundControl
cd ~/qgc/
gnome-terminal --tab -- ./QGroundControl.AppImage 
cd



