#!/bin/bash
#
# Runs gazebo simulation

cd
source ~/colcon_botsy/scripts/setup_ros_gazebo.bash

ros2 launch gazebo_ros gazebo.launch.py
